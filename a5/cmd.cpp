/**SERGEO MELLOW 50030322 PLEASE USE test5.html...it made me so happy when i saw it work
  ARIEL READ THESE COMMENTS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * *****************************************************************************
 * file name : cmd.cpp
 * author    : SERGEI MELLOW AND ARIEL AYBAR   
 * description : definitions of the three commands
 * *****************************************************************************
 */
#include <iostream>
#include <stack>
#include <map>
#include <fstream>
#include <sstream>
#include <cstdlib>   // for exit()
#include <string>

#include "cmd.h"
#include "Lexer.h"
#include "term_control.h"
#include "error_handling.h"

using namespace std;

// -----------------------------------------------------------------------------
// if you have any global variable, their definitions should go here
// -----------------------------------------------------------------------------
// int global_int; for example
/*
	
            (.,------...__
         _.'"             `.
       .'  .'   `, `. `.    `
      . .'   .'/''--...__`.  \
     . .--.`.  ' "-.     '.  |
     ''  .'  _.' .())  .--":/
     ''(  \_\      '   (()(
     ''._'          (   \ '
     ' `.            `--'  '
      `.:    .   `-.___.'  '
       `.     .    _  _  .'
         )       .____.-'
       .'`.        (--..
     .' \  /\      / /  `.
   .'    \(  \    /|/     `.
 .'           \__/          `.
/      |        o      |      \
       |               |      |
*/
/**   NOT ALL OF THE CODE/COMMENTS/FUNCTIONS APPLY THAT ARE BELOW BUT I LEFT IT IN TO CHECK WHAT DOES!!!!!!!!!!!!!!!!!!!!!!!
      LOTS OF CODE STILL NEEDS TO BE EDITED, DELETED, AND UNDERSTOOD AND EXAMPLES ONLINE LOOK AT THEM!
 * -----------------------------------------------------------------------------
 *  check whether the given file is well-formed
 *  note that the file stream and the error_msg are passed by reference
 *  - read the input file one line at a time
 *  - tokenize it into a stream of TAGs and IDENTs
 *  - push & pop TAGs from a stack appropriately to see if they match
 *  - store an error message in error_msg if there was an error
 *
 *  - we assume that ifs is opened and valid
 *
 *  - there are three common types of errors you should try to handle:
 *    1. a token is invalid, i.e. on a line there's a < but no closing >
 *    2. a TAG is not found from the set of valid tags, such as
 *       <invalidtag>
 *       </invalidtag>
 *       <some thing weird>
 *    3. the tags are valid but not nested correctly, here we have to use a
 *    stack
 *
 *  - you must not close ifs in this function
 * -----------------------------------------------------------------------------
 */
bool is_well_formed(ifstream& ifs, string& error_msg) {
    // SERGEI MELLOW 50030322
	string tempString,line;
	Token tok;
	Lexer lexer;	 
 while (getline(ifs, line)) {
 lexer.set_input(line);
 while (lexer.has_more_token()) {
 tok = lexer.next_token();
 switch (tok.type) {
 case STRING:     // read examples do we need to implement string?!??!?!?! and we also need an int thingy and a vector of vectors?
 break;
 case IDENT:
 break;
 case ERRTOK:
 cout << "Syntax error on this line\n"<<line<<"\n";
 return false;
 case ENDTOK:
 cout <<"Last token\n";
 break;
 default:
 cout << "broken code is broken\n";
 }
 }
 }
return true;
else
{
cout<<"\nstack had items remaing please close all your tags\n";
return false;
}
}

/*
                           o
                             .-""|
                             |-""|
                                 |   ._--+.
                                .|-""      '.
                               +:'           '.
                               | '.        _.-'|
                               |   +    .-"   J
            _.+        .....'.'|    '.-"      |
       _.-""   '.   ..'88888888|     +       J''..
    +:"          '.'88888888888;-+.  |    _+.|8888:
    | \         _.-+888888888_." _.F F +:'   '.8888'....
     L \   _.-""   |8888_.-"  _." J J J  '.    +88888888:
     |  '+"        |_.-"  _.-"    | | |    +    '.888888'._''.
   .'8L  L         J  _.-"        | | |     '.    '.88_.-"    '. 
  :888|  |         J-"            F F F       '.  _.-"          '.
 :88888L  L     _+  L            J J J          '|.               '; 
:888888J  |  +-"  \ L          _.+.|.+.          F '.          _.-" F
:8888888|  L L\    \|      _.-"    '   '.       J    '.     .-"    |
:8888888.L | | \    ', _.-"              '.     |      "..-"      J'.
:888888: |  L L '.    \     _.-+.          '.   :+-.     |        F88'.
:888888:  L | |   \    ;.-""     '.          :-"    ":+ J        |88888:
:888888:  |  L L   +:""            '.    _.-"     .-" | |       J:888888:
:888888:   L | |   J \               '.-'     _.-'   J J        F :888888:
 :88888:    \ L L   L \             _.-+  _.-'       | |       |   :888888:
 :888888:    \| |   |  '.       _.-"   |-"          J J       J     :888888:
 :888888'.    +'\   J    \  _.-"       F    ,-T"\  | |     .-'      :888888:
  :888888 '.     \   L    +"          J    /  | J  J J  .-'        .'888888:
   :8888888 :     \  |    |           |    F  '.|.-'+|-'         .' 8888888:
    :8888888 :     \ J    |           F   J     '...           .' 888888888:
     :8888888 :     \ L   |          J    |      \88'.''.''''.' 8888888888:
      :8888888 :     \|   |          |  .-'\      \8888888888888888888888:
       :8888888 '.    J   |          F-'  .'\      \8888888888888888888.'
        :88888888 :    L  |         J     : 8\      \8888888888888888.'
         :88888888 :   |  |        .+  ...' 88\      \8888888888.''.'
          :88888888 :  J  |     .-'  .'    8888\      \'.'''.'.' 
           :88888888 :  \ |  .-'   .' 888888888.\    _-'     
           :888888888 :  \|-'     .' 888888888.' \_-"
            '.88888888'..         : 8888888.' 
              :88888888  ''''.''.' 88888888:  hs       
              :8888888888888888888888888888:
               :88888888888888888888888888:
                :888888888888888888888888:
                 ''.8888888888888...'.'''
                    '''''......''*
                                                                                      
                                                                                          
                                  `,;;,;: .,;;:                                           
                                `@@#@@#@@@@@@#@@##',                                      
                             `;#@#@@@@@@@@@@@@@@@@@+':                                    
                            '@#@@@@@@@@@@@@@@@@@@@@@@#@#:                                 
                          `##@@@@@@@@@@@@@@@@@@@@@@@@#@@##                                
                        `:#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                               
                       `#@#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#                              
                      .'#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#'                             
                      +@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#@`                            
                     +#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#                            
                    ::#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#;                           
                   :+@@@@@@@@@@@@@@@@#@@#@#@@@@@@@@@@@@@@@@@@#@                           
                  ,@#@@@@@@@@@@@@@@@@#@#. ;@@@@@@@@@@@@@@@@@@#@.                          
                 .@@@@@@@@@@@@@@@#++;.`    .@@@@@@@@@@@@@@@@@###`                         
                 #@@@@@@@@@@@@#+`            .'@@@@@@@@@@@@@@@@@`                         
                `@@@@@@@@@@@@@,               `@@@@@@@@@@@@@@@@@'                         
                `@#@@#@##++,                  `@@@@@@@@@@@@@@@@@@                         
                 @@@@@@.                      `@@@@@@@@@@@@@@@@@#                         
                 #@@@@.                       `@@@@@@@@@@@@@@@@@@`                        
                 `#@@;            `           `@@@@@@@@@@@@@@@@@@,                        
                  .#@`         `:               +#@@@@@@@@@@@@@@@:                        
                   #'        ;`                  '@#@@@@@@@@@@@@#+                        
                   #@   :,  .'                    ###@@@@@@#+    +                        
                  `#; .,  ``` .#@+#`               @@@@@@@@@ :,,:`.                       
                   @; , `:: ``@#@#@#''''.          .@@@@@@#:``   ,'                       
                   +,  ,@@ ,`'@#@###@@@@@@:         #@@@@@:`;     .                       
                   ', ,@@@;,      ;@@@@@@@#:`       '@@@#:  `     '                       
                    ';#@@ ```            '+` `      ;@#@:    `    .                       
                    +#@@`  `,              :        .@@#    ,,: ,'`                       
                   .@#@.           ;:;,  ::.',      .+':    #    ,`                       
                   @@@#.::``.  :  ''#@'',`.,,              ``    ,.                       
                   @#+`   `:'  ;   :    ```                `.   ,;.                       
                   @@`   :::.  `       :`                  `;   ,;,                       
                   .:  '@''#':     .:;.                     : `` `.                       
                   `. ##` ` `,                              ';`   '                       
                    :;;   ,;:`                              :     `                       
                     ;.';; '.                                    ;                        
                     ,.`  : '                                   ;                         
                     , `  . :      .`.;:                    ``.+@                         
                     ;   ; ``     `  ,  :`                  ;` ++                         
                     `  `` '         ,   ,`                 ,  @,                         
                    .   : ,,    `,; '`    ,`    .           , .@`                         
                    :   ' :,   :`          ;    ;           ; ,#                          
                    ,   , ..#  ,            ;,  :           + ;@                          
                     `   ,           ````   : ` ,           + ;#                          
                     ;  :       `.:,``::;#:   : ``            ''                          
                      ` :     ;,` `'@+@:;#'@``, `             @:                          
                      ; `   '` `+@:;, , `:.+.,: :             #                           
                       , , ;;@@+ , .``#'@@@# ,: :             ,                           
                       . .`,'+,``. '###@#@#+  :`              ,                           
                     `  ' :`,;#:'##+@@@;'@`,  ::              ,                           
                        `: ,`''@##@@:,,;, ;   `;              :                           
                          , : .:@+`.;'`  :`   `.            ` :                           
                          `. ',`       .+    ..             : :                           
                           .```;:`..:;,      ';          ` '  :                           
                            : '`            , ,           , ` .                           
                             ;.            ``.           ,`    '                          
                              `.           , ,           .     `.                         
                              ;`                        '       '                         
                              ;                         `       ,                         
                              ;             ,                   `.                        
                              :           `;                     '+                       
                               ,        ` ;                      ' :                      
                               ,,       .'                        ```                     
                                `+:  .;,                          ' ,``                   
                                ,+ .;                             +  `;                   
                               `, .  '                             ,   .,                 
                               ;  ;   :                            ,     '                
                                   `                               ,              


WE WILL OWN THIS CASTLE IF WE  DO THE TASKS BELOW
 * -----------------------------------------------------------------------------
 *  TO BE IMPLEMENTED BY SERGEI MELLOW AND ARIEL AYBAR
 *  - check whether the 2 given file is well-formed 
 *  - display the DATABASE
 *  - you must not close ifs in this function
 *  - this function return nothing
Maximum score of 100 points, broken down as follows.

    5 points if a Makefile is provided in the directory and it works properly.
    5 points if the exit command works.
    5 points if an unknown command (other than display, nljoin, smjoin, and exit) is handled properly, i.e. "Unknown Command" message is printed without the program terminating.
    5 points if your code is organized properly   
    5 points if your code has a consistent style following the rough guideline above
    25 points if the display command without sortedby option works.

    (You should be done with the above this weekend! That's half the assignment.)
    15 points if the display command with sortedby works, using the randomized quick sort code that I (will) give.
    15 points if the nljoin command works
    15 points if the smjoin command works and sorting is done with randomized quick sort
    2.5 points if input errors are handled properly: no matching column type, or missing column data or extra column data.
    2.5 points if your code can detect and report (without quitting) that a smjoin is typed over a non-unique column 
 * -----------------------------------------------------------------------------
 */		
void print_well_formed_file(ifstream& ifs) {
//SERGEI MELLOW 50030322
Token token;
Lexer lexer;
while(getline(ifs, line))
{
lexer.set_input(line);
while(lexer.has_more_token())
{
token=lexer.next_token();
switch (token.type) {
 case STRING:
 if (token.value[0] != '/')
{
}
//this closes the else if its a close STRING TAG THAT MIGHT NEED IMPLEMETATIOM
 break;
 case IDENT:
 cout<<token.value<<" ";
 break;
 case ERRTOK:
 cout<<"how is there an error token here?\n";
 break;
 case ENDTOK:
 cout <<"Last token\n";
 break;
 default:
 cout << "broken code is broken\n";
} //end of switch
} //end of while look for more token
} //end of big look for more lines
} //closing the print_well_formed...
/*
 * -----------------------------------------------------------------------------
 *  call lexer to scan for the expected next token: a TAG that stores the
 *  file name
 *  - only returns success if TAG is the only token seen
 *  - the file name, if it is there is returned via setting the input referenced
 *  string    THINK ABOUT IT...
 * -----------------------------------------------------------------------------
 */
bool parse_input(Lexer lexer, string& file_name) {
    Token file_name_tok;

    if (!lexer.has_more_token() || 
        (file_name_tok = lexer.next_token()).type != TAG)
        return false;

    if  (lexer.has_more_token()) 
        return false;

    file_name = file_name_tok.value;
    return true;
}

/**
 * -----------------------------------------------------------------------------
 *  display the database   NEEDS IMPLEMENTATION
 * -----------------------------------------------------------------------------
 */
void display(Lexer cmd_lexer) {
    string file_name;

    if (!parse_input(cmd_lexer, file_name)) {
        error_return("Syntax error: display <filename>");
        return;
    }

    ifstream ifs(file_name.c_str());
    string error_msg;
    if (ifs) {
        if (!is_well_formed(ifs, error_msg)) {
            error_return(error_msg);
        } else {
            ifs.clear();            // clear EOF flag
            ifs.seekg(0, ios::beg); // go back to the very beginning
            print_well_formed_file(ifs);
  	    ifs.close();
        }
    } else {
        error_return("Can't open " + file_name + " for reading");
    }
    ifs.close();
}

/**
 * -----------------------------------------------------------------------------
 *  validate the input file WHICH IS GOING TO BE A DATABASE INPUTS I GUESS?
 * -----------------------------------------------------------------------------
 */
void validate(Lexer cmd_lexer) {
    string file_name; // pattern and file name

    if (!parse_input(cmd_lexer, file_name)) {
        error_return("Syntax error: validate <filename>");
        return;
    }

    ifstream ifs(file_name.c_str());
    string error_msg;
    if (ifs) {
        if (!is_well_formed(ifs, error_msg)) {
            error_return(error_msg);
        } else {
            cout << term_fg(CYAN) << "The file " << file_name 
                << " is valid\n" << term_fg();
        }
    } else {
        error_return("Can't open " + file_name + " for reading");
    }
    ifs.close();
}


/**
 * -----------------------------------------------------------------------------
 *  simply quit  WHICH IS GONNA QUIT OUR DATABASE PROGRAM AKA BROWSER
 * -----------------------------------------------------------------------------
 */
void bye(Lexer l) {
    if (l.has_more_token()) {
        error_return("Usage: exit");
        return;
    }
    exit(0);
}

