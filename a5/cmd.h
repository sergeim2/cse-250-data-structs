/*
 * *****************************************************************************
 * file name  : cmd.h
 * author     : Hung Ngo
 * description: codes to implement user's commands
 *              illustrates the user of function pointers for late binding
 *              this implementation is overkill for this particular example,
 *              I wanted to illustrate this idea so that you can reuse it in
 *              later assignments which will require more sophisticated
 *              commands
 * *****************************************************************************
 */


#ifndef _CMD_H
#define _CMD_H

#include <string>
#include "Lexer.h"

/**
 * cmd_handler_t is a function pointer type, pointing to a function that takes
 * a Lexer object and returns nothing.
 */
typedef void (*cmd_handler_t)(Lexer);

void validate(Lexer);
void display(Lexer);
void bye(Lexer);

#endif
