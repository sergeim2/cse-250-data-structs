/**
 * ****************************************************************************
 * file: browser.cpp
 * author: Hung Q. Ngo
 * description: 
 * - a command line interface for displaying a toy HTML-like file
 * usage: browser
 * > validate <file name>
 * > display <file name>
 * > exit
 * where validate checks whether the file 'file name' is well-formed
 * and display prints the file to the screen
 *
 * new concepts illustrated in my implementation (which is more complicated
 * than necessary for this particular assignment)
 * - parsing and the Lexer class
 * - function pointers
 * - using 'map' class and the delegate pattern
 * ****************************************************************************
 */
#include <iostream>
#include <fstream>
#include <string>   
#include <map>   

#include "term_control.h"
#include "error_handling.h"
#include "Lexer.h"
#include "cmd.h"

using namespace std;


// -----------------------------------------------------------------------------
// the program doesn't take any argument
// -----------------------------------------------------------------------------
int main(int argc, char **argv) {
    if (argc != 1) error_quit("Browser doesn't take any argument");

    const char *usage_msg = 
	 "Usage: display <file name>\n"
	 "       nljoin  <file name>\n"
	 "       smjoin  <file name>\n"
	 "       exit/quit/bye\n";
    map<string, cmd_handler_t> commands;
    commands["nljoin"] = &nljoin;
    commands["smjoin"] = &smjoin;
    commands["display"] = &display;
    commands["quit"] = &bye;
    commands["exit"] = &bye;
    commands["bye"] = &bye;

    string line; Token tok; Lexer lexer;

    while (cin) {
	cout<<">";
        getline(cin, line); 
        lexer.set_input(line);

        if (!lexer.has_more_token()) continue;

        tok = lexer.next_token();
        if (tok.type == IDENT) {
            if (commands.find(tok.value) != commands.end()) {
                commands[tok.value](lexer);
                continue;
            }
        }
        note(usage_msg);
    }

    return 0; // 0 indicates 'success', this is a Unix thing
}
