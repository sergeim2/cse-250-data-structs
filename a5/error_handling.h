/*
 * *****************************************************************************
 * file name : error_handling.h
 * author    : Hung Ngo
 * description : error reporting functions
 * *****************************************************************************
 */


#ifndef _ERROR_HANDLING_H
#define _ERROR_HANDLING_H

#include <string>

void error_quit(std::string);
void error_return(std::string);
void print_warning(std::string);
void note(std::string);

#endif
