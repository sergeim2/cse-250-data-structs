// ****************************************************************************
// main.cpp
// author: Hung Q. ngo
// Description: get a user's command of the form
// - is 10
// - ms 20
// where 'is' stands for insertion sort
// and   'ms' stands for merge sort
// the numbers 10 or 20 are the list size, a random singly linked list of size
// 10 or 20 (or what ever number you entered) will be created and passed to
// the appropriate sorting function
// after sorting, the result is printed out and the list is freed
// ****************************************************************************
#include <iostream>
#include <map>
#include <sstream>
#include <cstdlib>  // for rand() and srand()
#include <ctime>    // for time()

#include "list_sort.h"
#include "Lexer.h"
#include "error_handling.h"
#include "term_control.h"

using namespace std;

// -----------------------------------------------------------------------------
// prototypes and typedefs
// -----------------------------------------------------------------------------
Node* random_list(size_t n); // return a random list of size n
typedef void (*cmd_t)(Lexer);
void is_cmd(Lexer); // insertion sort
void ms_cmd(Lexer); // merge sort
void bye(Lexer);     // simply quit
void prompt();

/**
 * -----------------------------------------------------------------------------
 * main body
 * -----------------------------------------------------------------------------
 */
int main() {
    Lexer lexer; string line; Token tok;
    map<string,cmd_t> cmd_map;
    cmd_map["exit"] = &bye;
    cmd_map["bye"]  = &bye;
    cmd_map["is"]   = &is_cmd; // call insertion sort
    cmd_map["ms"]   = &ms_cmd; // call merge sort

    cout << term_cc(YELLOW)
        << "List Sort Program. Version 0.7\n"
        << " Report bugs to no body, as noone is using this program\n";

    while (cin) {
        prompt(); getline(cin, line); lexer.set_input(line);
        if (!lexer.has_more_token()) continue;
        tok = lexer.next_token();
        if (tok.type != IDENT) { error_return("Syntax error\n"); continue; }

        if (cmd_map.find(tok.value) != cmd_map.end()) {
            try {
                cmd_map[tok.value](lexer);
            } catch (runtime_error &rterror) {
                error_return(rterror.what());
            }
        } else {
            error_return("Unknown command");
        }
    }
    return 0;

}

/**
 * -----------------------------------------------------------------------------
 * create a random list
 * -----------------------------------------------------------------------------
 */
Node* random_list(size_t n) {
    if (n == 0) return NULL;

    srand(static_cast<unsigned int>(time(0)));
    Node* head = NULL;
    for (size_t i=0; i<n; i++) {
        head  = new Node(rand() % n, head);
    }
    return head;
}

/**
 * -----------------------------------------------------------------------------
 * call the insertion sort routine
 * -----------------------------------------------------------------------------
 */
void is_cmd(Lexer lexer) {
    Token tok = lexer.next_token();
    if (tok.type != INTEGER || lexer.has_more_token()) {
        throw runtime_error("Syntax error: is <number>");
    } else {
        size_t n;
        stringstream ss(tok.value);
        ss >> n;
        Node* head = random_list(n);
        cout << term_cc(CYAN) << "BEFORE: " << term_cc(); print_list(head); 
        head = insertion_sort(head);
        cout << term_cc(RED)  << "AFTER : " << term_cc(); print_list(head); 
        free_list(head);
    }
}

/**
 * -----------------------------------------------------------------------------
 * call the merge sort routine
 * -----------------------------------------------------------------------------
 */
void ms_cmd(Lexer lexer) {
    Token tok = lexer.next_token();
    if (tok.type != INTEGER || lexer.has_more_token()) {
        throw runtime_error("Syntax error: is <number>");
    } else {
        size_t n;
        stringstream ss(tok.value);
        ss >> n;
        Node* head = random_list(n);
        cout << term_cc(CYAN) << "BEFORE: " << term_cc(); print_list(head); 
        head = merge_sort(head);
        cout << term_cc(RED)  << "AFTER : " << term_cc(); print_list(head); 
        free_list(head);
    }
}

/**
 * -----------------------------------------------------------------------------
 * print the list
 * -----------------------------------------------------------------------------
 */
void print_list(Node* ptr) {
    while (ptr != NULL) {
        cout << ptr->key << " ";
        ptr = ptr->next;
    }
    cout << endl;
}

/**
 * -----------------------------------------------------------------------------
 * terminates the program, ignores all parameters
 * -----------------------------------------------------------------------------
 */
void bye(Lexer lexer) {
    if (lexer.has_more_token()) {
        throw runtime_error("Syntax error: use bye/exit/quit");
    } else {
        exit(0);
    }
}

/**
 * -----------------------------------------------------------------------------
 * just print a prompt.
 * -----------------------------------------------------------------------------
 */
void prompt() {
    cout << term_cc(BLUE) << "> " << term_cc() << flush;
}
