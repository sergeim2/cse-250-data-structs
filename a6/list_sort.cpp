// *****************************************************************************
// list_sort.cpp
// Author: SERGEI MELLOW
// Description: implement the two sorting algorithms on singly linked lists
// - please feel free to add more functions to this file
// - this is the only file you can modify
// *****************************************************************************
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "list_sort.h"
using namespace std;

/**
 * -----------------------------------------------------------------------------
 * the selection sort algorithm
 * -----------------------------------------------------------------------------
 */
  Node* insertion_sort(Node* head) {
  Node* tempN=head->next;	
  Node* tempP=head;
  int c,temp;
  while(tempN !=  NULL)
  {
    c=tempP->key;
    while(tempN!=tempP)
    {
  	temp=tempP->key;
	if(tempN->key<=tempP->key)
  	  {
  		tempP->key=tempN->key;
  		tempN->key= temp;
	  }
        tempP=tempP->next;
    }
    tempP=head;
    tempN=tempN->next;
  }
  return tempP; // and you can return a different value!
                 // the way it is right now no sorting is done
}

/**
 * -----------------------------------------------------------------------------
 * the merge sort algorithm
 *lots of credits to links below. Had to change code quite a bit though
 *http://stackoverflow.com/questions/7685/merge-sort-a-linked-list
 *http://www.dontforgettothink.com/2011/11/23/merge-sort-of-linked-list/
 * -----------------------------------------------------------------------------
 */
Node* merge_sort(Node* head) {
    // your code goes here
    if(head == NULL || head->next == NULL) { return head; }
    Node* middle = getMiddle(head);      //get the middle of the list
    Node* sHalf = middle->next; 
    middle->next = NULL;   //split the list into two halfs

    return merge(merge_sort(head),merge_sort(sHalf));  //recurse on that
}

//Merge subroutine to merge two sorted lists
Node* merge(Node* a, Node* b) {
    Node* dHead  = new Node();
    Node*  curr=dHead;
    Node* dummyHead=dHead; 
    curr = dummyHead;
    while(a !=NULL && b!= NULL) {
        if(a->key <= b->key) { curr->next = a; a = a->next; }
        else { curr->next = b; b = b->next; }
        curr = curr->next;
    }
    curr->next = (a == NULL) ? b : a;
    return dummyHead->next;
}

//Finding the middle element of the list for splitting
Node* getMiddle(Node* head) {
    if(head == NULL) { return head; }
    Node* slow=head; 
    Node* fast=head; 
    while(fast->next != NULL && fast->next->next != NULL) {
        slow = slow->next; fast = fast->next->next;
    }
    return slow;
}

// *****************************************************************************
// DO NOT MODIFY BEYOND THIS POINT
// *****************************************************************************

/**
 * -----------------------------------------------------------------------------
 * free the memory of all nodes starting from ptr down
 * -----------------------------------------------------------------------------
 */
void free_list(Node* ptr) {
    Node* temp;
    while (ptr != NULL) {
        temp = ptr;
        ptr = ptr->next;
        delete temp;
    }
}

