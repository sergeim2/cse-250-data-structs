/**SERGEO MELLOW 50030322 PLEASE USE test5.html...it made me so happy when i saw it work
 * *****************************************************************************
 * file name : cmd.cpp
 * author    : Hung Ngo
 * description : definitions of the three commands
 * *****************************************************************************
 */
#include <iostream>
#include <stack>
#include <map>
#include <fstream>
#include <sstream>
#include <cstdlib>   // for exit()
#include <string>

#include "cmd.h"
#include "Lexer.h"
#include "term_control.h"
#include "error_handling.h"

using namespace std;

// -----------------------------------------------------------------------------
// if you have any global variable, their definitions should go here
// -----------------------------------------------------------------------------
// int global_int; for example

/**
 * -----------------------------------------------------------------------------
 *  TO BE IMPLEMENTED BY YOU
 *  check whether the given file is well-formed
 *  note that the file stream and the error_msg are passed by reference
 *  - read the input file one line at a time
 *  - tokenize it into a stream of TAGs and IDENTs
 *  - push & pop TAGs from a stack appropriately to see if they match
 *  - store an error message in error_msg if there was an error
 *
 *  - we assume that ifs is opened and valid
 *
 *  - there are three common types of errors you should try to handle:
 *    1. a token is invalid, i.e. on a line there's a < but no closing >
 *    2. a TAG is not found from the set of valid tags, such as
 *       <invalidtag>
 *       </invalidtag>
 *       <some thing weird>
 *    3. the tags are valid but not nested correctly, here we have to use a
 *    stack
 *
 *  - you must not close ifs in this function
 * -----------------------------------------------------------------------------
 */
bool is_well_formed(ifstream& ifs, string& error_msg) {
    // SERGEI MELLOW 50030322
	stack<string> myStack;
	string tempString,line;
	Token tok;
	Lexer lexer;	 
 while (getline(ifs, line)) {
 lexer.set_input(line);
 while (lexer.has_more_token()) {
 tok = lexer.next_token();
 switch (tok.type) {
 case TAG:
 if (tok.value[0] != '/')
{
if(tok.value=="red" || tok.value== "green" || tok.value== "yellow" || tok.value== "blue" || tok.value== "magenta" || tok.value== "cyan" || tok.value== "dim" || tok.value== "underline" || tok.value== "bright")
myStack.push(tok.value);
else
{
cout<<"invalid tag on this line:"<<line<<endl;
return false;
}
 cout << "OPEN TAG: " << tok.value << endl;
}
 else
{
 cout << "CLOSE TAG: " << tok.value.substr(1) << endl;
if(myStack.top()==tok.value.substr(1))
myStack.pop();
else
{
cout <<"Error:"<<myStack.top()<<" !=" << tok.value.substr(1)<<" Please close your tags correct and don't mismatch\n"<<"error on this line:\n"<<line<<endl;
return false;
}
}
 break;
 case IDENT:
 break;
 case ERRTOK:
 cout << "Syntax error on this line\n"<<line<<"\n";
 return false;
 case ENDTOK:
 cout <<"Last token\n";
 break;
 default:
 cout << "broken code is broken\n";
 }
 }
 }
if(myStack.size()==0)
return true;
else
{
cout<<"\nstack had items remaing please close all your tags\n";
return false;
}
}

/**
 * -----------------------------------------------------------------------------
 *  TO BE IMPLEMENTED BY YOU
 *  - check whether the given file is well-formed
 *  - display the file
 *  - you must not close ifs in this function
 *  - this function return nothing
 * -----------------------------------------------------------------------------
 */		
void print_well_formed_file(ifstream& ifs) {
//SERGEI MELLOW 50030322
string line;
stack<string> myStack;
stack<string> atrStack;
Token token;
Lexer lexer;
while(getline(ifs, line))
{
lexer.set_input(line);
while(lexer.has_more_token())
{
token=lexer.next_token();
switch (token.type) {
 case TAG:
 if (token.value[0] != '/')
{
if(token.value=="red" || token.value== "green" || token.value== "yellow" || token.value== "blue" || token.value== "magenta" || token.value== "cyan")
{
myStack.push(token.value);
if(token.value=="red")
cout<<term_fg(RED);
if(token.value=="green")
cout<<term_fg(GREEN);
if(token.value=="yellow")
cout<<term_fg(YELLOW);
if(token.value=="blue")
cout<<term_fg(BLUE);
if(token.value=="magenta")
cout<<term_fg(MAGENTA);
if(token.value=="cyan")
cout<<term_fg(CYAN);
}//end of color assigments of term
if(token.value=="dim"||token.value=="underline"||token.value=="bright")
{
atrStack.push(token.value);//stacking opening attrib
if(token.value=="dim")
cout<<term_attrib(DIM);
if(token.value=="underline")
cout<<term_attrib(UNDERLINE);
if(token.value=="bright")
cout<<term_attrib(BRIGHT);  
}//end of attribute assignments
}//end of open tag
//start of closing tags code
if(token.value=="/bright" || token.value=="/underline" || token.value=="/dim")
{
//cout<<token.value<<"<CLOSING ";
cout<<term_attrib();
atrStack.pop();//Please still work
if(!myStack.empty())
{
if(myStack.top()=="red")
cout<<term_fg(RED);
if(myStack.top()=="green")
cout<<term_fg(GREEN);
if(myStack.top()=="yellow")
cout<<term_fg(YELLOW);
if(myStack.top()=="blue")
cout<<term_fg(BLUE);
if(myStack.top()=="magenta")
cout<<term_fg(MAGENTA);
if(myStack.top()=="cyan")
cout<<term_fg(CYAN);
}
if(!atrStack.empty())
{
if(atrStack.top()=="bright")
cout<<term_attrib(BRIGHT);
if(atrStack.top()=="underline")
cout<<term_attrib(UNDERLINE);
if(atrStack.top()=="dim")
cout<<term_attrib(DIM);
}
}
if(token.value=="/red"||token.value=="/green"||token.value=="/magenta"||token.value=="/cyan"||token.value=="/blue"||token.value=="/yellow")
{
cout<<term_fg(); //everything resets?
myStack.pop();//popping a color tag since its not an attrib
if(!myStack.empty())
{
if(myStack.top()=="red")
cout<<term_fg(RED);
if(myStack.top()=="green")
cout<<term_fg(GREEN);
if(myStack.top()=="yellow")
cout<<term_fg(YELLOW);
if(myStack.top()=="blue")
cout<<term_fg(BLUE);
if(myStack.top()=="magenta")
cout<<term_fg(MAGENTA);
if(myStack.top()=="cyan")
cout<<term_fg(CYAN);
}
if(!atrStack.empty())
{
if(atrStack.top()=="bright")
cout<<term_attrib(BRIGHT);
if(atrStack.top()=="underline")
cout<<term_attrib(UNDERLINE);
if(atrStack.top()=="dim")
cout<<term_attrib(DIM);
}
}
//this closes the else if its a close tag
 break;
 case IDENT:
 cout<<token.value<<" ";
 break;
 case ERRTOK:
 cout<<"how is there an error token here?\n";
 break;
 case ENDTOK:
 cout <<"Last token\n";
 break;
 default:
 cout << "broken code is broken\n";
} //end of switch
} //end of while look for more token
} //end of big look for more lines
} //closing the print_well_formed...
/*
 * -----------------------------------------------------------------------------
 *  call lexer to scan for the expected next token: a TAG that stores the
 *  file name
 *  - only returns success if TAG is the only token seen
 *  - the file name, if it is there is returned via setting the input referenced
 *  string
 * -----------------------------------------------------------------------------
 */
bool parse_input(Lexer lexer, string& file_name) {
    Token file_name_tok;

    if (!lexer.has_more_token() || 
        (file_name_tok = lexer.next_token()).type != TAG)
        return false;

    if  (lexer.has_more_token()) 
        return false;

    file_name = file_name_tok.value;
    return true;
}

/**
 * -----------------------------------------------------------------------------
 *  display file if well-formed
 * -----------------------------------------------------------------------------
 */
void display(Lexer cmd_lexer) {
    string file_name;

    if (!parse_input(cmd_lexer, file_name)) {
        error_return("Syntax error: display <filename>");
        return;
    }

    ifstream ifs(file_name.c_str());
    string error_msg;
    if (ifs) {
        if (!is_well_formed(ifs, error_msg)) {
            error_return(error_msg);
        } else {
            ifs.clear();            // clear EOF flag
            ifs.seekg(0, ios::beg); // go back to the very beginning
            print_well_formed_file(ifs);
  	    ifs.close();
        }
    } else {
        error_return("Can't open " + file_name + " for reading");
    }
    ifs.close();
}

/**
 * -----------------------------------------------------------------------------
 *  validate the input file
 * -----------------------------------------------------------------------------
 */
void validate(Lexer cmd_lexer) {
    string file_name; // pattern and file name

    if (!parse_input(cmd_lexer, file_name)) {
        error_return("Syntax error: validate <filename>");
        return;
    }

    ifstream ifs(file_name.c_str());
    string error_msg;
    if (ifs) {
        if (!is_well_formed(ifs, error_msg)) {
            error_return(error_msg);
        } else {
            cout << term_fg(CYAN) << "The file " << file_name 
                << " is valid\n" << term_fg();
        }
    } else {
        error_return("Can't open " + file_name + " for reading");
    }
    ifs.close();
}


/**
 * -----------------------------------------------------------------------------
 *  simply quit
 * -----------------------------------------------------------------------------
 */
void bye(Lexer l) {
    if (l.has_more_token()) {
        error_return("Usage: exit");
        return;
    }
    exit(0);
}

