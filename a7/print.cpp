// *****************************************************************************
// file name : print.cpp
// author    : Sergei Mellow
// description : implement the two tree printing routines
// *****************************************************************************
#include <iostream>
#include <string>
#include <iomanip>
#include <algorithm> // for max()
#include <map>
#include <vector>
#include <queue>
#include <deque>
#include "BTree.h"
#include "term_control.h"
#include <utility>
#include <algorithm>
#include <list>
using namespace std;
void ver(BTNode<string>* p,string indent);
void hor(BTNode<string>* p,string indent);
//int max_depth(BTNode<string>* n);
//void prt(BTNode<string>* n);
void Print(BTNode<string>* x, int  id);

// feel free to change the following notice
string version = 
    "UB Tree Program. Version 0.1\n"
    " Author: Sergei Mellow\n"
    " Report bugs to sergeime@buffalo.edu\n";

/*
 * -----------------------------------------------------------------------------
 * print a given tree vertically
 * -----------------------------------------------------------------------------
 */
void vertical_print(BTNode<string>* root) {
	cout<<"Horizontal Print"<<endl;
	if(root!=NULL)
	ver(root,"");
}


/*
 * -----------------------------------------------------------------------------
 * print a given tree horizontally
 * -----------------------------------------------------------------------------
 */
void horizontal_print(BTNode<string>* root) {
	cout<<"Horizontal Print"<<endl;
	if(root!=NULL)
	hor(root,"");
 }

/*
 * -----------------------------------------------------------------------------
 * print a given tree symmetrically
 * -----------------------------------------------------------------------------
 */

void symmetric_print(BTNode<string>* root) {
 cout<<"Horizontal Print"<<endl;
         if(root!=NULL)
         ver(root,"");

}


void ver(BTNode<string>* p, string indent)
{
    if(p != NULL) {
        cout<< indent <<p->payload<<"\n ";
        string newIndent = indent + "    ";

	if(p->right) ver(p->right, newIndent);
        if(p->left) ver(p->left, newIndent);
    }
}

void hor(BTNode<string>* p, string indent)
  {
     if(p != NULL) {
	 if(p->left!=NULL)
         cout<< indent <<p->payload<<"                   ";
         string newIndent = indent + "\n\n\n\n\\n";

         if(p->right) ver(p->right, newIndent);
         if(p->left) ver(p->left, newIndent);
     }
 }

